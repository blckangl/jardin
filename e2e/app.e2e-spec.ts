import { JardinPage } from './app.po';

describe('jardin App', () => {
  let page: JardinPage;

  beforeEach(() => {
    page = new JardinPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
